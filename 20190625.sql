﻿USE ciclistas;

/*
  HOJA 1 - Consultas de selección 1
  */

-- 1. edades de los ciclistas sin repetidos
SELECT DISTINCT c.edad FROM ciclista c;

-- 2 edades de los ciclistas de Artiach
SELECT DISTINCT edad FROM ciclista c WHERE c.nomequipo='Artiach';


-- 3 edades de los ciclistas de Artiach o de Amore Vita
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Artiach' OR c.nomequipo='Amore Vita'; -- acepta las comillas dobles
  -- con el OR es un poco tosco

  -- operador extendido IN
    SELECT DISTINCT 
    c.edad 
    FROM ciclista c 
    WHERE c.nomequipo IN ('Artiach','Amore Vita');


   -- operador de conjuntos UNION
    SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Artiach'
      UNION
    SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Amore Vita';


-- 4 dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30
SELECT c.dorsal FROM ciclista c WHERE c.edad<25  OR c.edad>30;

-- 26/6/2019
  -- con el operador in
  SELECT c.dorsal FROM ciclista c WHERE c.edad NOT IN (25,26,27,28,29,30) ;

  -- con la union
      -- c1: listar ciclistas menores de 25 
       SELECT c.dorsal FROM ciclista c WHERE c.edad <25;
      -- c2: listar ciclistas menores de 25 
       SELECT c.dorsal FROM ciclista c WHERE c.edad >30;
      -- final
        SELECT c.dorsal FROM ciclista c WHERE c.edad<25
          UNION
        SELECT c.dorsal FROM ciclista c WHERE c.edad>30;

  -- con el not between
  SELECT c.dorsal FROM ciclista c WHERE c.edad NOT BETWEEN 25 AND 30;



-- 5 dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que solo sean de Banesto
SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo='Banesto';


-- 6 nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8
SELECT DISTINCT c.nombre FROM ciclista c WHERE LENGTH(c.nombre)>8;
SELECT DISTINCT c.nombre FROM ciclista c WHERE CHAR_LENGTH(c.nombre)>8;

SELECT "texto",23;
SELECT CHAR_LENGTH("la casa");
SELECT CHAR_LENGTH(c.nombre), c.nombre FROM ciclista c;
/*
  length mide en bites
  char_length mide en caracteres
*/
SELECT CHAR_LENGTH("la casa"), LENGTH("la casa");
SELECT CHAR_LENGTH(c.nombre) longitud, c.nombre  FROM ciclista c;

SELECT CURDATE(), now(); -- curdate devuelve la fecha, now devuelve la fecha y la hora
SELECT date(NOW()); -- date necesita argumento que sea fecha y hora

SELECT DISTINCT c.edad FROM ciclista c;
SELECT COUNT(edad) FROM ciclista c; -- cuenta las edades donde hay algo escrito
SELECT COUNT(DISTINCT edad) FROM ciclista c; -- cuenta las edades donde no hay repetidos
SELECT COUNT(*) FROM (SELECT DISTINCT c.edad FROM ciclista c) c1; -- cuenta aunque haya nulos


-- 7 nombre y dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayusculas
--   que debe mostrar el nombre en mayusculas
SELECT c.dorsal, upper(c.nombre) nombre_mayusculas FROM ciclista c;

-- 8 ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa
SELECT DISTINCT l.dorsal FROM lleva l WHERE l.código='MGE';

SELECT * FROM (SELECT DISTINCT l.dorsal FROM lleva l WHERE l.código='MGE') c1 JOIN ciclista c USING(dorsal);


-- 9 nombre de los puertos cuya altura sea mayor que 1500
SELECT nompuerto FROM puerto p WHERE p.altura >1500;

-- 10 dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000
SELECT p.dorsal FROM puerto p WHERE p.pendiente >8 OR p.altura BETWEEN  1800 AND 3000;

-- 11 dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000
SELECT p.dorsal FROM  puerto p WHERE p.pendiente > 8 AND p.altura BETWEEN 1800 AND 3000;